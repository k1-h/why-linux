#Why GNU/Linux?

***By:***

**Keyvan Hedayati**

**Sameer Rahmani**

===
#Powerful & Flexible

![](img/flexible.jpg)
---
## Super Computer Market
![Super Compters](img/Operating_systems_used_on_top_500_supercomputers.svg)
---
## Server Market
![Servers](img/servers.png)
---
## Many choices for many needs
![](img/distros.png) <!-- .element: style="height: 600px;" -->
---
## Many Distributions & Desktops

* Ubuntu/Mint for Desktop
* Redhat/CentOS/Debian for Server
* Backtrack/Kali for Hack/Security
* Lubuntu/Xubuntu/Puppy for old PCs/Slow hardware
* Archlinux/Gentoo for GNU/Linux Geeks
* Fedora for Cutting Edge
* SliTaz/Raspbian for Embded/Raspberry Pi

Note:
Gnome/Unity/Cinnamon for simplicity and usability
KDE for beauty and customizblity
Xfce/LXDE/Mate for
Openbox/Awesome for extensible, highly configurable

---
## My experience for flexiblity

![Thin Client](img/Aleutia_E3_Medium_Client_in_Hand.jpg)

* We built a 15-20 MB linux for Thin Clients
* Netboot via DHCP/PXE/TFTP

===
# No Malware

![](img/malware-table.jpg)
---
## Malware Hell in Windows

Read about it

[Who is Making All This Malware — and Why?](http://www.howtogeek.com/183642/who-is-making-all-this-malware-and-why/)

![](img/credit-cards.jpg)

Note:
Why would a piece of malware want to destroy your software and force you to reinstall Windows?
keylogger
connecting to a remote server and waiting for instructions
Botnets and Ransomware

---
## It's not only about that

*"GNU/Linux don't have malware because it does not have many users"*
My classmate said

**Desktop OS Share**

![](img/desktop.png) <!-- .element: style="height: 400px;" -->

---
## Secure by design

* Package managers and software repositories
* Normal users have limited access
* Nothing can run without user permission
* Android has many users and no app can ruin the system
* More info? Read [HTG Explains: Why You Don’t Need an Antivirus On GNU/Linux](http://www.howtogeek.com/135392/htg-explains-why-you-dont-need-an-antivirus-on-linux-and-when-you-do/)

---
## Server Security

* SELinux & AppArmor for sandboxing compromised apps
* LXC (LinuX Containers)
* It's Open Source, You don't need Mi©ro$oft to fix a BUG!
  * [UK government pays £5.5 million for XP security updates](http://www.howtogeek.com/186754/microsoft-is-still-making-security-updates-for-windows-xp-but-you-cant-have-them/)
* No backdoor, No NSA
  * But things became complicated after Heartbleed

Note:
LXC is from LUG!

===
# Some Other Features

![](img/linuxusers.jpg)

---
## Freedom

*You control software, Software doesn't control you*

![](img/RYF.jpg) <!-- .element: style="height: 500px;" -->

---
## Stable

* No Reinstall
* No Slow Down
* No Restart

Note:
Vahids talk about win reinstall after bad software
I have installed this linux almost a year ago

---
## Package Manager
###### (once more)

* Install apps with a single command/click
* Easily update whole system
* Don't need to install bunch software after installing OS
* Don't need to install Drivers

---
## Community

* LUGs
* PUGs
* ZConf
* SFD
* PyCon
* Ubuntu Release Parties

===
# Heads Up!
#### Install GNU/Linux with open eyes

---
## You'll need to use Google, a lot

![](img/google.png)

*(If you have a problem in linux and there's not a solution for that it means your problem is wrong)*

---
## You might need to be good with English

![](img/english.jpg)

---
## Find an alternative for your proprietary software
#### If you failed
**Use Wine & Playonlinux & Steam**

---
## You'll gonna need terminal

![](img/terminal.png) <!-- .element: style="height: 500px;" -->

---
## More you read, more easy to migrate

![](img/man.png)

===
# Final Notes

---
## GNU/Linux isn't Windows

![](img/linux_windows.jpg)

*For more info read [GNU/Linux is NOT Windows](http://linux.oneandoneis2.org/LNW.htm)*

---
## I have Windows and it works!
### Why should I migrate?

*(I had this feeling 3 years ago)*

---
#### I learned a lesson:
## Migration is GOOD!

* Windows to GNU/Linux
* PHP to Python
* Crap editors to Emacs
* Bash to ZSH

===
## Hope you liked it

* Keyvan Hedayati
  * [me@k1h.ir](mailto:me@k1h.ir)
  * [@K1Hedayati on Twitter](https://twitter.com/K1Hedayati/)
* Sameer Rahmani
  * [lxsameer@gnu.org](mailto:lxsameer@gnu.org)
  * [@lxsameer on Twitter](https://twitter.com/lxsameer/)
* View this presetation online:
  * [k1h.ir/why-linux](http://k1h.ir/why-linux)
